﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class YellowBird : Bird{
    [SerializeField]
    public float _boostForce = 100;
    public bool _canBoost = true;

    public void Boost(){
        if (State == BirdState.Thrown && _canBoost){
            RigidBody.AddForce(RigidBody.velocity * _boostForce);
            _canBoost = false;
        }
    }

    public override void OnTap(){
        Boost();
    }
}
