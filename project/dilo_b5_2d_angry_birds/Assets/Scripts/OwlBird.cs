﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class OwlBird : Bird{
    [SerializeField]
    public float _diveForce = 100;
    public bool _canDive = true;
    public float _massRatio = 10;

    public void Dive(){
        if (State == BirdState.Thrown && _canDive){
            // stops mid air then dive straight down
            RigidBody.velocity = Vector2.zero;
            RigidBody.mass *= _massRatio;
            RigidBody.AddForce(-transform.up * _diveForce * _massRatio, ForceMode2D.Impulse);
            _canDive = false;
        }
    }

    public override void OnTap(){
        Dive();
    }
}
